class Employee{
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return this.name;
    }

    getAge() {
        return this.age;
    }

    getSalary() {
        return this.salary;
    }

    setName(name) {
        this.name = name;
    }

    setAge(age) {
        this.age = age;
    }

    setSalary(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, listLang){
        super(name, age, salary);
        this.lang = listLang;
    }

    getSalary() {
        return this.salary * 3;
    }
}

programmer1 = new Programmer('Katty', '32', 1000, ['eng', 'ukr', 'arm']);
programmer2 = new Programmer('Machile', '41', 1500, ['eng', 'rus', 'afr']);
programmer3 = new Programmer('Alex', '34', 2000, ['eng', 'ukr', 'yid']);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

console.log(programmer1.getSalary());
console.log(programmer2.getSalary());
console.log(programmer3.getSalary());